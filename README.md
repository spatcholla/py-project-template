# Spatcholla's Python Project Template

This is a [cookiecutter](https://github.com/audreyr/cookiecutter) template for a base Python project. It utilizes popular libraries alongside Make and Graphviz to fully automate all development and deployment tasks.

## Features

* `pyproject.toml` for managing dependencies and package metadata
* `Makefile` for automating common [development tasks](https://gitlab.com/Spatcholla/py-project-template/blob/main/%7B%7Bcookiecutter.project_name%7D%7D/CONTRIBUTING.md):
    * Installing dependencies with `poetry`
    * Automatic formatting with `isort` and `black`
    * Static analysis with `pylint`
    * Type checking with `mypy`
    * Docstring styling with `pydocstyle`
    * Running tests with `pytest`
    * Building documentation with `mkdocs`
* Tooling to launch an IPython session with automatic reloading enabled
* Visual Studio Code settings for debugging, launching, linting and formatting

## Usage

Install `cookiecutter` and generate a project:

```shell
pip install cookiecutter
cookiecutter gl:Spatcholla/py-project-template -f
```

Cookiecutter will ask you for some basic info (your name, project name, python package name, etc.) and generate a base Python project for you.

## Updates

Run the update tool, which is generated inside each project:

```shell
bin/update
```
