# Overview

{{cookiecutter.project_short_description}}

This project was generated with [cookiecutter](https://github.com/audreyr/cookiecutter) using [spatcholla/py-project-template](https://gitlab.com/Spatcholla/py-project-template).

[![Coverage Status](https://img.shields.io/coveralls/{{cookiecutter.gitlab_username}}/{{cookiecutter.gitlab_repo}}.svg)](https://coveralls.io/r/{{cookiecutter.gitlab_username}}/{{cookiecutter.gitlab_repo}})
[![Scrutinizer Code Quality](https://img.shields.io/scrutinizer/g/{{cookiecutter.gitlab_username}}/{{cookiecutter.gitlab_repo}}.svg)](https://scrutinizer-ci.com/g/{{cookiecutter.gitlab_username}}/{{cookiecutter.gitlab_repo}})

## Setup

### Requirements

* Python {{cookiecutter.python_major_version}}.{{cookiecutter.python_minor_version}}+

### Installation

Install it directly into an activated virtual environment:

```shell
pip install {{cookiecutter.project_name}}
```

or add it to your [Poetry](https://poetry.eustace.io/) project:

```shell
poetry add {{cookiecutter.project_name}}
```

## Usage

After installation, the package can imported:

```shell
$ python
>>> import {{cookiecutter.package_name}}
>>> {{cookiecutter.package_name}}.__version__
```
